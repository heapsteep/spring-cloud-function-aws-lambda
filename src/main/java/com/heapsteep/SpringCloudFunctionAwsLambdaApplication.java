package com.heapsteep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudFunctionAwsLambdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudFunctionAwsLambdaApplication.class, args);
	}

}
